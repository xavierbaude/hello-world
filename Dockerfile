FROM centos:latest
LABEL maintainer="Xavier Baude - https://github.com/xavierbaude"
ARG CI_COMMIT_SHA

RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-* && sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

RUN  if [ -z $CI_COMMIT_SHA ];then CI_COMMIT_SHA=null;export CI_COMMIT_SHA;fi && \
     echo "hello world, tag is $CI_COMMIT_SHA" > /tmp/index.html && \
     yum update -y && \
     yum clean all && \
     rm -rf /var/cache/yum
RUN yum install -y python36

WORKDIR /tmp

EXPOSE 8080
USER 65535

ENTRYPOINT ["python3", "-m", "http.server", "8080"]
